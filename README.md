# RestAssuredWithCucumber Master

Download the project to local machine using git clone ... Local machine should have java and maven installed.

Project folder: 
"\src\test\java\features" -> for feature file
"\src\test\java\stepsdefs" -> for steps defination


once you download the project , go to RestAssuredWithCucumber-master and run "mvn test" command to run the test cases.

After executing test cases it will generate the report on Report folder under RestAssuredWithCucumber-master directory.

Report file location:

"RestAssuredWithCucumber-master\reports\html-reports\cucumber-html-reports"

